window.addEventListener("load", comenzar, false);

function comenzar() {
   var miBoton = document.getElementById("dame_ubicacion");

   miBoton.addEventListener("click", obtener, false);
}

function obtener() {
   var parametros = {
      enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 60000
   };

   navigator.geolocation.getCurrentPosition(mostrar_posicion, gestion_errores, parametros);
}

function mostrar_posicion(posicion) {
   var ubicacion = document.getElementById("ubicacion");
   /*
   var miUbicacion = "";

   miUbicacion += "Latitud: " + posicion.coords.latitude + "<br>";
   miUbicacion += "Longitud: " + posicion.coords.longitude + "<br>";
   miUbicacion += "Exactitud: " + posicion.coords.accuracy + "<br>";
*/
   var mimapa = "http://maps.google.com/maps/api/staticmap?center=" + posicion.coords.latitude + "," + posicion.coords.longitude + "," +
      "&zoom=12&size=400x400&sensor=false&markers=" + posicion.coords.latitude + "," + posicion.coords.longitude;

   ubicacion.innerHTML = "<img src='" + mimapa + "'/>";
}

function gestion_errores(error) {
   //alert("Ha habido un error " + error.code + " " + error.message);

   if (error.message === "User denied Geolocation") {
      alert("Debes permitir el uso de la geolocalización en tu navegador");
   }
}