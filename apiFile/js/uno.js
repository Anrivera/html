window.addEventListener("load", iniciar, false);

var zonadatos;
var btn;

function iniciar() {
   zonadatos = document.getElementById("zonadatos");
   var archivos = document.getElementById("archivos");

   archivos.addEventListener("change", procesar, false);
}

function procesar(e) {
   var archivos = e.target.files;
   zonadatos.innerHTML = "";

   var mi_archivo = archivos[0];

   if (!mi_archivo.type.match(/image/)) {
      alert("Selecciona una image, por favor");
   } else {
      zonadatos.innerHTML += "Nombre del archivo: " + mi_archivo.name + "<br/>";
      zonadatos.innerHTML += "Tamaño del archivo: " + Math.round(mi_archivo.size / 1024) + " kb <br/>";


      var lector = new FileReader();

      //  lector.readAsText(mi_archivo, "iso-8859-1");
      lector.readAsDataURL(mi_archivo);
      lector.addEventListener("load", mostrarWeb, false);
   }
}

function mostrarWeb(e) {
   var resultado = e.target.result;
   //zonadatos.innerHTML = resultado;
   zonadatos.innerHTML += "<img src='" + resultado + "' width='85%'/>";
}