window.addEventListener("loadl", init, false);

function init() {
   var btn = document.getElementById("boton");

   btn.addEventListener("click", enviar, false);

   trabajador = new SharedWorker("js/trabajadorDos.js");

   trabajador.port.addEventListener("message", recibido, false);
   trabajador.port.start();
}

function enviar() {
   var nombre = document.getElementById("nombre").value;

   trabajador.port.postMessage(nombre);
}

function recibido(e) {
   alert(e.data);
}