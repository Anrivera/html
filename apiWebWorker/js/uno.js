window.addEventListener("load", init, false);

function init() {
   zonadatos = document.getElementById("zonadatos");
   var btn = document.getElementById("boton");

   btn.addEventListener("click", enviar, false);

   trabajador = new Worker("js/trabajador.js");
   trabajador.addEventListener("message", recibido, false);
}

function enviar() {
   var nombre = document.getElementById("nombre").value;

   trabajador.postMessage(nombre);
}

function recibido(e) {
   console.log(e);
   zonadatos.innerHTML = e.data;
}