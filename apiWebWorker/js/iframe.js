window.addEventListener("load", init, false);

function init() {
   trabajador = new SharedWorker("js/trabajadorDos.js");
   trabajador.port.addEventListener("message", recibido, false);
   trabajador.port.start();
}

function recibido(e) {
   zonadatos = document.getElementById("zonadatos");

   zonadatos.innerHTML = e.data;
}