window.addEventListener("load", init, false);

function init() {
   var btn = document.getElementById("boton");
   btn.addEventListener("click", enviar, false);

   window.addEventListener("message", recibir, false);

   recepcion = document.getElementById("zonarecepcion");

}

function enviar() {
   var mensaje = document.getElementById("mensajes").value;
   var iframe = document.getElementById("iframe");

   iframe.contentWindow.postMessage(mensaje, "http://apicommunication.local");
}

function recibir(e) {
   recepcion.innerHTML += e.data;

}