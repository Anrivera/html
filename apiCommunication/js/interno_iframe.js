window.addEventListener("load", init, false);

function init() {
   window.addEventListener("message", receptor, false);
}

function receptor(e) {
   var zonamensajes = document.getElementById("zonamensajes");
   console.log(e.origin);
   if (e.origin == "http://apicommunication.local") {

      zonamensajes.innerHTML += "Mensaje desde: " + e.origin + "<br/>";
      zonamensajes.innerHTML += "Mensaje: " + e.data + "<br/>";

      e.source.postMessage("Mensaje recibido correctamente <br/>", e.origin);
   } else {
      e.source.postMessage("Mensaje erróneo");
   }
}