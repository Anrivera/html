window.addEventListener("load", init, false);

function init() {
   zonadatos = document.getElementById("zonadatos");

   var boton = document.getElementById("archivos");

   boton.addEventListener("change", subirArchivos, false);
}

function subirArchivos(e) {
   var archvos = e.target.files;
   var archivo = archivos[0];
   var url = "procesar.php";
   var solicitud = new XMLHttpRequest();
   var subida = solicitud.upload;

   subida.addEventListener("loadstart", comienzaBarra, false);
   subida.addEventListener("progress", estadoBarra, false);
   subida.addEventListener("load", mostrar, false);

   solicitud.open("POST", url, true);

   var datos = new FormData();

   datos.append("archivo", archivo);
   solicitud.send(datos);
}

function comienzaBarra() {
   zonadatos.innerHTML = "<progress value='0' max='100'></progress";
}

function estadoBarra() {
   var procentaje = parseInt(e.loaded / e.total * 100);
   var barraProgreso = zonadatos.querySelector("progress");

   barraProgreso.value = procentaje;
   zonaprogreso.innerHTML = procentaje + " %";
}

function mostrar(e) {
   zonadatos.innerHTML = "Terminado!!!";
}