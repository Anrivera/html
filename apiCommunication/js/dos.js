window.addEventListener("load", Init, false);

function Init() {
   zonadatos = document.getElementById("zonadatos");
   var btn = document.getElementById("boton");

   btn.addEventListener("click", enviarDatos, false);
}

function enviarDatos() {
   nombre = document.getElementById("elnombre").value;
   apellido = document.getElementById("elapellido").value;

   var datos = new FormData();

   datos.append("nombre", nombre);
   datos.append("apellido", apellido);

   var url = "procesar.php";
   var solicitud = new XMLHttpRequest();

   solicitud.addEventListener("load", mostrar, false);
   solicitud.open("POST", url, true);
   solicitud.send(datos);
}

function mostrar(e) {
   zonadatos.innerHTML = e.target.responseText;
}