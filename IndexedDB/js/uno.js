window.addEventListener("load", iniciar, false);

var zonadatos;
var btn;
function iniciar() {
   zonadatos = document.getElementById("zonadatos");
   btn = document.getElementById("grabar");

   btn.addEventListener("click", agregarObjeto, false);

   var solicitud = indexedDB.open("miBase"); //Crear base

   solicitud.onsuccess = function (e) {
      db = e.target.result;
   }

   //Crear tabla
   solicitud.onupgradeneeded = function (e) {
      db = e.target.result;
      db.createObjectStore("gente", { keyPath: "clave" });;
   }
}

function agregarObjeto() {
   var clave = document.getElementById("clave").value;
   var titulo = document.getElementById("texto").value;
   var fecha = document.getElementById("fecha").value;

   var transaccion = db.transaction(["gente"], "readwrite");
   var almacen = transaccion.objectStore("gente");
   var agregar = almacen.add({ clave: clave, titulo: titulo, fecha: fecha })

   agregar.addEventListener("success", mostrar, false);
   /*
      document.getElementById("clave").value = "";
      document.getElementById("texto").value = "";
      document.getElementById("fecha").value = "";
   */
}

function mostrar() {
   zonadatos.innerHTML = "";

   var transaccion = db.transaction(["gente"], "readonly");
   var almacen = transaccion.objectStore("gente");
   var cursor = almacen.openCursor();

   cursor.addEventListener("success", mostrarDatos, false);
}

function mostrarDatos(e) {
   var cursor = e.target.result;

   if (cursor) {
      zonadatos.innerHTML += "<div>" + cursor.value.clave + " - " + cursor.value.titulo + " - " + cursor.value.fecha + " </div>";
      cursor.continue();
   }
}