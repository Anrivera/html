var myvideo, reproducir, barra, progreso, maximo, bucle;
maximo = 600;

function comenzar() {
  myvideo = document.getElementById("myvideo");
  reproducir = document.getElementById("reproducir");
  barra = document.getElementById("barra");
  progreso = document.getElementById("progreso");

  reproducir.addEventListener("click", hacerClick, false);
  barra.addEventListener("click", adelantando, false);
}

function hacerClick() {
  if (!myvideo.paused && !myvideo.ended) {
    myvideo.pause();
    reproducir.innerHTML = "Play";
  } else {
    myvideo.play();
    reproducir.innerHTML = "Pause";
    bucle = setInterval(estado, 30);
  }
}

function estado() {
  if (!myvideo.ended) {
    var total = parseInt((myvideo.currentTime * maximo) / myvideo.duration);
    progreso.style.width = total + "px";
  }
}

function adelantando(posicion) {
  if (!myvideo.paused && !myvideo.ended) {
    var ratonx = posicion.pageX - barra.offsetLeft;
    var nuevoTiempo = (ratonx * myvideo.duration) / maximo;
    myvideo.currentTime = nuevoTiempo;
    progreso.style.width = ratonx + "px";
  }
}

window.addEventListener("load", comenzar, false);
