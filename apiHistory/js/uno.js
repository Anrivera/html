window.addEventListener("load", init, false);

function init() {
   zonadatos = document.getElementById("zonadatos");
   url = document.getElementById("url");

   url.addEventListener("click", cambiarUrl, false);
   window.addEventListener("popstate", nuevaUrl, false);
   window.replaceState(1, null);
}

function cambiarUrl() {
   /*
   zonadatos.innerHTML += "Estás en la página #2";

   window.history.pushState(null, null, "dos.html");
   */
   mostrar(2);
   window.history.pushState(2, null, "dos.html")
}

function nuevaUrl(e) {
   mostrar(e.state);
}

function mostrar(actual) {
   zonadatos.innerHTML = "Estás en la página: " + actual;
}