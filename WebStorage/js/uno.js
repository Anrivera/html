window.addEventListener("load", comenzar, false);

function comenzar() {
   var btn = document.getElementById("grabar");

   btn.addEventListener("click", itemNuevo, false);
}

function itemNuevo() {
   var clave = document.getElementById("clave").value;
   var valor = document.getElementById("valor").value;

   localStorage.setItem(clave, valor);
   //localStorage[clave] = valor
   leer_monstrar(clave);

   document.getElementById("clave").value = "";
}

function leer_monstrar(clave) {
   var zonadatos = document.getElementById("zonadatos");
   zonadatos.innerHTML = "<div><button onclick='eliminarTodo()'>Eliminar Todos</button></div>";
   //var elvalor = localStorage.getItem(clave);
   //var elvalor = localStorage[clave];


   // zonadatos.innerHTML = "";

   for (i = 0; i < localStorage.length; i++) {
      var clave = localStorage.key(i);
      var elvalor = localStorage.getItem(clave);
      zonadatos.innerHTML += '<div>Clave: ' + clave + ' -- ' + 'Valor: ' + elvalor + '<br/><button onclick="eliminarItem(\'' + clave + '\')">Eliminar</button></div>';
   }
}

function eliminarTodo() {
   if (confirm("Estas segudo?")) {
      localStorage.clear();
      leer_monstrar();
   }
}

function eliminarItem(clave) {
   if (confirm("Estás seguro?")) {
      localStorage.removeItem(clave);
      leer_monstrar();

   }

}